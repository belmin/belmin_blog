+++
# Introduce the blog.
widget = "starter.blog.intro"
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear in.

title = "A bit of social science and a lot of code"
subtitle = ""

[design.background]
  # Background color.
  color = "#42b3c2"

  # Text color (true=light or false=dark).
  text_color_light = true
+++
