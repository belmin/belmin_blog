---
# Display name
title: Camille Belmin

# Is this the primary user of the site?
superuser: true

# Role/position
role: PhD Candidate

# Organizations/Affiliations
organizations:
- name: Potsdam Institute for Climate Impact Research (PIK)
  url: "https://www.pik-potsdam.de/en"

# Short bio (displayed in user profile at end of posts)
bio: I am a PhD candidate at PIK. My research interests include distributed robotics, mobile computing and programmable matter.

#interests:
#- Artificial Intelligence
#- Computational Linguistics
#- Information Retrieval

#education:
#  courses:
#  - course: PhD in Artificial Intelligence
#    institution: Stanford University
#    year: 2012
#  - course: MEng in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2009
#  - course: BSc in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'about/#contact'  # For a direct email link, use "mailto:belmin@pik-potsdam.de".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/BelminCamille
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.pik-potsdam.de/belmin
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
#email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
#user_groups:
#- Researchers
#- Visitors
---

I am a PhD Student at Potsdam Institute for Climate Impact Research in the Future Lab *Social Metabolism and Impacts* and at Humboldt University of Berlin. I am studying the relationship between energy use, population growth and sustainability in low-income countries. My research lies at the intersection of different fields: energy sociology, demography, social metabolism and gender studies. I am working with survey data, statistics, micro-simulation modelling, mostly in R.  
